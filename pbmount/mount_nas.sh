#!/bin/bash


# ======================================================================
# C O N F I G U R A T I O N   -   P L E A S E   C U S T O M I Z E
# ======================================================================

# Specify the username.
USERNAME="john"

# Specify the password.
PASSWORD="top_secret"

# Specify the local user ID (UID) for your user (use the command 'id -u').
LOCAL_UID=1000

# Specify the local group ID (GID) for your user (use the command 'id -g').
LOCAL_GID=1000

# Specify the charset used to convert local path names to and from Unicode.
CHARSET="utf8"

# Specify if the shares should be mounted in read-write (rw) or in read-only (ro) mode. 
ACCESS_TYPE="rw"

# Specify the directory mode (directory permissions).
DIR_MODE=0777

# Specify the file mode (file permissions).
FILE_MODE=0666

# Specify IP address of the NAS (host).
NAS_IP="192.168.0.111"

# Specify the NAS shares which have to be mounted.
NAS_SHARES="Audio Books Download Images Videos"

# Specify the mount point (without the share name).
MOUNT_POINT="/media"

# Specify the full path to the file manager.
FILE_MANAGER="/usr/bin/thunar"


# ======================================================================
# T I T L E   M E S S A G E
# ======================================================================

# Clear the screen.
clear

# Show an information message.
echo
echo "MOUNT ALL NAS SHARES..."
echo "-----------------------"
echo


# ======================================================================
# L O O P   T H R O U G H   A L L   S H A R E S
# ======================================================================

# Loop through all the shares.
for share in $NAS_SHARES
do


  # ====================================================================
  # C R E A T E   A C T U A L   M O U N T P O I N T
  # ====================================================================

  # Check if the mountpoint does not exist.
  if [ ! -d "$MOUNT_POINT"/"$share" ]; then

    # Show an information message.
    echo "Create the mountpoint $MOUNT_POINT/$share ..."

    # Create the mountpoint.
    sudo mkdir -p "$MOUNT_POINT"/"$share"

  fi


  # ====================================================================
  # M O U N T   A C T U A L   S H A R E
  # ====================================================================

  # Check if the actual share is not already mounted.
  if [ $(mountpoint "$MOUNT_POINT"/"$share" | grep "not" | wc -l) == 1 ]; then

    # Show an information message.
    echo "Mount the share $MOUNT_POINT/$share ..."

    # Mount the actual share.
    sudo mount -t cifs -o username="$USERNAME",password="$PASSWORD",uid=$LOCAL_UID,gid=$LOCAL_GID,iocharset="$CHARSET",$ACCESS_TYPE,dir_mode=$DIR_MODE,file_mode=$FILE_MODE //"$NAS_IP"/"$share" "$MOUNT_POINT"/"$share"

    echo

  fi

done


# ======================================================================
# S H O W   A L L   M O U N T E D   S H A R E S
# ======================================================================

# Check if the filemanager exist
if [ -f "$FILE_MANAGER" ]; then
   
  # Start the filemanager and display the mounted shares
  "$FILE_MANAGER" "$MOUNT_POINT" &

fi

