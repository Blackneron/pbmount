#!/bin/bash


# ======================================================================
# C O N F I G U R A T I O N   -   P L E A S E   C U S T O M I Z E
# ======================================================================

# Specify IP address of the NAS.
NAS_IP="192.168.0.111"

# Specify the NAS shares which have to be mounted.
NAS_SHARES="Audio Books Download Images Videos"

# Specify the mount point (without the share name).
MOUNT_POINT="/media"


# ======================================================================
# T I T L E   M E S S A G E
# ======================================================================

# Clear the screen.
clear

# Show an information message.
echo
echo "UNMOUNT ALL NAS SHARES..."
echo "-------------------------"
echo


# ======================================================================
# L O O P   T H R O U G H   A L L   S H A R E S
# ======================================================================

# Loop through all the shares.
for share in $NAS_SHARES
do

  # Check if the mountpoint does exist.
  if [ -d "$MOUNT_POINT"/"$share" ]; then


    # ==================================================================
    # U N M O U N T   A C T U A L   S H A R E
    # ==================================================================

    # Check if the actual share is already mounted.
    if [ $(mountpoint "$MOUNT_POINT"/"$share" | grep "not" | wc -l) == 0 ]; then

      # Show an information message.
      echo "Unmount share $MOUNT_POINT/$share ..."

      # Unmount the actual share.
      sudo umount "$MOUNT_POINT"/"$share"

    fi


    # ==================================================================
    # D E L E T E   A C T U A L   M O U N T P O I N T
    # ==================================================================

    # Check if the actual share is not mounted.
    if [ $(mountpoint "$MOUNT_POINT"/"$share" | grep "not" | wc -l) ]; then

      # Check if the mountpoint does exist.
      if [ -d "$MOUNT_POINT"/"$share" ]; then

        # Show an information message.
        echo "Remove mountpoint $MOUNT_POINT/$share ..."

        # Delete the mountpoint.
        sudo rmdir "$MOUNT_POINT"/"$share"
        echo

      fi

    fi

  fi

done

