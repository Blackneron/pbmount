# PBmount Scripts - README #
---

### Overview ###

The **PBmount** scripts (Bash) can be used to mount or unmount a list of NAS shares. Different options can be specified in the configuration section of the scripts.

### Screenshots ###

![PBmount - Mount NAS shares](development/readme/pbmount1.png "PBmount - Mount NAS shares")

![PBmount - Unmount NAS shares](development/readme/pbmount2.png "PBmount - Unmount NAS shares")

### Setup ###

* Copy the directory **pbmount** to your computer.
* Be sure that the scripts are not available for everyone because of the passwords!
* Open the two scripts in your favorite editor and customize the configuration sections.
* Make the two scripts executable (chmod +x).
* The script **mount_nas.sh** is used to mount NAS shares.
* The script **umount_nas.sh** is used to backup unmount NAS shares.
* Use the scripts from the command line.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBmount** scripts are licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
